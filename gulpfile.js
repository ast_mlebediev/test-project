var gulp = require('gulp');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
/*var cssnano = require('gulp-cssnano');*/


var src_sass = 'app/sass/**/*.scss';
var src_css = 'app/css';

gulp.task('sass', function () {
    return gulp.src(src_sass)
        .pipe(sass())
        .pipe(gulp.dest(src_css))
});

gulp.task('default', function () {
    gulp.watch(src_sass, ['sass'])
});

gulp.task('css', function () {
 var processors = [autoprefixer];
 return gulp.src(src_css+'/main.css')
 .pipe(postcss(processors))
 .pipe(gulp.dest(src_css));
});



