$(document).ready(function() {

    /*slider for items*/
    $('.product-photo').slick({
        mobileFirst: true,
        dots: false,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 768,
            settings: 'unslick'
        }]
    });

    // read more text
    $(".details-button_item").click(function() {
        $(".details-text").fadeToggle();
    });
});